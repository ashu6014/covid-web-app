## Covid-19 Themed Web Application in React
This is a web application developed using React library to provide up-to-date information about the Covid-19 pandemic. It provides worldwide/countrywide statistics of the number of cases, recoveries and deaths due to Covid-19 disease. The latest information regarding the vaccine can be found in the Vaccines section in the web application. The data used in this webapp is facilitated by disease.sh open Apis. 


## Quick Start

npm run start

### Dependencies
- @types/react                  16.8.18
- bootstrap                     4.3.1
- chart.js                      2.8.0
- history                       4.9.0
- node-sass                     4.12.0
- react                         16.8.6
- react-chartjs-2               2.7.6
- react-dom                     16.8.6
- react-notification-alert      0.0.12
- react-router-dom              5.0.0
- react-scripts                 3.0.1
- reactstrap                    8.0.0
- ajv                           6.10.0
- jquery                        3.4.1
- @types/googlemaps             3.36.0
- typescript                    3.4.5
- react-router                  5.0.0

## File Structure

Within the download you'll find the following directories and files:

```
├── LICENSE.md
├── README.md
├── jsconfig.json
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
└── src
    ├── index.js
    ├── virus-logo.svg
    ├── routes.js
    ├── components
    │   ├── Footer
    │   │   └── Footer.jsx
    │   ├── Navbars
    │   │   └── DemoNavbar.jsx
    │   └── Sidebar
    │       └── Sidebar.jsx
    ├── layouts
    │   └── Admin.jsx
    ├── variables
    │   ├── charts.jsx
    │   ├── general.jsx
    │   └── icons.jsx
    ├── views
    │   ├── Dashboard.jsx
    │   ├── Symptoms.jsx
    │   ├── Map.jsx
    │   ├── Vaccines.jsx
    │   ├── LineGraph.jsx
    │   
    └── assets
        ├── css
        │   ├── paper-dashboard.css
        │   ├── paper-dashboard.css.map
        │   └── paper-dashboard.min.css
        ├── demo
        ├── fonts
        ├── github
        ├── img
        │   └── <flat-icons-svgs-here>
        └── scss
            ├── paper-dashboard
            │   ├── cards
            │   ├── mixins
            │   ├── plugins
            │   └── react
            │       ├── custom
            │       └── react-differences.scss
            └── paper-dashboard.scss
```


