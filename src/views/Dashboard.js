import React, { useEffect, useState } from "react";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Dropdown } from 'reactstrap';
import { Line, Pie } from "react-chartjs-2";
import "leaflet/dist/leaflet.css";
import Map from './Map';
import LineGraph from "./LineGraph";
import "./../assets/css/Map.css";
import numeral from "numeral";
import { sortData, prettyPrintStat } from "./../util";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Table,
  Col,
} from "reactstrap";
import {
  MenuItem,
  FormControl,
  Select,
  
  CardContent,
} from "@material-ui/core";
// core components
import {
  dashboard24HoursPerformanceChart,
  dashboardEmailStatisticsChart,
  dashboardNASDAQChart,
} from "variables/charts.js";

const Dashboard = () => {

  const [country, setInputCountry] = useState("worldwide");
  const [countryInfo, setCountryInfo] = useState({});
  const [countries, setCountries] = useState([]);
  const [mapCountries, setMapCountries] = useState([]);
  const [tableData, setTableData] = useState([]);
  const [casesType, setCasesType] = useState("deaths");
  const [mapCenter, setMapCenter] = useState({ lat: 34.80746, lng: -0.4796 });
  const [mapZoom, setMapZoom] = useState(2);

  useEffect(() => {
    const targetUrl = "https://disease.sh/v3/covid-19/all";
    fetch(targetUrl)
      .then((response) => response.json())
      .then((data) => {
        setCountryInfo(data);
      });
  }, []);  
  
  useEffect(()=>{
      const getCountriesData = async ()=>{
        const targetUrl = "https://disease.sh/v3/covid-19/countries";
        await fetch(targetUrl)
        .then((response)=>response.json())
        .then((data)=>{        
          const countries = data.map((country) =>({
            name: country.country, //name of the country
            value: country.countryInfo.iso2 //iso code of the country
          }) )
          let sortedData = sortData(data);
          setMapCountries(data);
          setTableData(sortedData.slice(0,5));
          setCountries(countries);
        })
      }
      getCountriesData();      
    }, [])


    const onCountryChange = async (e) => {
      const countryCode = e.target.value;
      const targetUrl =
        countryCode === "worldwide"
          ? "https://disease.sh/v3/covid-19/all"
          : `https://disease.sh/v3/covid-19/countries/${countryCode}`;
      await fetch(targetUrl)
        .then((response) => response.json())
        .then((data) => {
          setInputCountry(countryCode);
          setCountryInfo(data);
          if(countryCode == "worldwide"){
            setMapCenter([34.80746, -0.4796]);
          }else{
            setMapCenter([data.countryInfo.lat, data.countryInfo.long]);
          }
          console.log("djsndunwdi", data.countryInfo.lat, data.countryInfo.long)
          setMapZoom(4);
        });
    };

   return (
      <>
      
        <div className="content">
        <Row>
        <Col lg="3" md="8" sm="8">
              <Card className="card-stats" >
                <CardBody>
                  <Row>
                  
                    <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                        <i style={{color:"#51bcda"}} className="nc-icon nc-globe" />
                      </div>
                      
                    </Col>
                    <Col md="8" xs="7">
                    <p className="card-category">Select Country</p>
                    <FormControl className="country-dropdown" >
  <Select  onChange={onCountryChange} value={country} style={{textTransform:'uppercase', fontWeight:600, fontFamily:"Montserrat, Helvetica Neue, Arial, sans-serif"}}>
    <MenuItem value="worldwide"><b>WORLDWIDE</b></MenuItem>
    {countries.map((country)=>(
         <MenuItem style={{textTransform:'uppercase', fontFamily:"Montserrat, Helvetica Neue, Arial, sans-serif"}}  value={country.value}>{country.name}</MenuItem> 
        ))}  
  </Select>
</FormControl>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    Sars-Cov-2 Statistics
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="8" sm="8">
              <Card style={{cursor:"pointer"}} className="card-stats" onClick={(e) => setCasesType("cases")} active={casesType === "cases"}>
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-vector text-warning" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Covid-19 Cases</p>
                        <CardTitle tag="p"> + {numeral(countryInfo.todayCases).format("0.0a")}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                  <i className="far fa-clock" /> {numeral(countryInfo.cases).format("0.0a")} Total
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="8" sm="8">
              <Card style={{cursor:"pointer"}} className="card-stats" onClick={(e) => setCasesType("recovered")} active={casesType === "recovered"}>
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-check-2 text-success" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Recoveries</p>
                        <CardTitle tag="p"> + {numeral(countryInfo.todayRecovered).format("0.0a")}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                  <i className="far fa-clock" /> {numeral(countryInfo.recovered).format("0.0a")} Total
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="8" sm="8">
              <Card style={{cursor:"pointer"}} className="card-stats" onClick={(e) => setCasesType("deaths")} active={casesType === "deaths"}>
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-sound-wave text-danger" />
                      </div> 
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Deaths</p>
                        <CardTitle tag="p"> + {numeral(countryInfo.todayDeaths).format("0.0a")}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                  <i className="far fa-clock" />{numeral(countryInfo.deaths).format("0.0a")} Total
                  </div>
                </CardFooter>
              </Card>
            </Col>
            
            
          </Row>
        <Row>
            <Col md="12">
              <Card>               
                <CardBody>
                  

<Map
          countries={mapCountries}
          casesType={casesType}
          center={mapCenter}
          zoom={mapZoom}
        />
                </CardBody>
                
              </Card>
            </Col>
          </Row>
         
          
          <Row>
            <Col md="6">
              <Card>
                <CardHeader>
                  <CardTitle tag="h5">{casesType.toLocaleUpperCase()} - 90 days</CardTitle>
                  <p className="card-category">Sars-Cov-2</p>
                </CardHeader>
                <CardBody>
                <LineGraph casesType={casesType} />
                </CardBody>
              </Card>
            </Col>
            <Col md="6">
              <Card className="card-chart">
                <CardHeader>
                  <CardTitle tag="h5">Most Affected Countries</CardTitle>
                </CardHeader>
                <CardBody>
                   <Table responsive>
                  <thead className="text-primary">
                    <tr>
                      <th>Country</th>
                      <th>Cases Count</th>
                    </tr>
                  </thead>
                  <tbody>
                  {tableData.map((country) => (
        <tr>
          <td>{country.country}</td>
          <td>
            {numeral(country.cases).format("0,0")}
          </td>
        </tr>
      ))}
                  </tbody>
                </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );

        }


export default Dashboard
