import React from "react";
import { Card, CardHeader, CardBody, CardTitle, Row, Col } from "reactstrap";

class Icons extends React.Component {
  render() {
    return (
      <>
       
        <div className="content">
          <Row>
            <Col md="12">
              <Card className="demo-icons">
                <CardHeader>
                  <CardTitle tag="h5">Most Common Symptoms</CardTitle>
                  
                </CardHeader>
                <CardBody className="all-icons">
                  <div id="icons-wrapper">
                    <section>
                      <ul>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/fever.svg')}></img>
          
                        {/* <i className="fas fa-thermometer-2 fa-2x" /> */}
                          <p>Fever</p>
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/cough.svg')}></img>
                        {/* <i className="fas fa-thermometer-2 fa-2x" /> */}
                          <p>Cough</p>
                        
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/headache.svg')}></img>
                          {/* <i className="nc-icon nc-alert-circle-i" /> */}
                          <p>Headache</p>
                          
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/fatigue.svg')}></img>
                          {/* <i className="nc-icon nc-align-center" /> */}
                          <p>Fatigue</p>
                        
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/chills.svg')}></img>
                          {/* <i className="nc-icon nc-align-left-2" /> */}
                          <p>Chills</p>
                        
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/loss.svg')}></img>
                          {/* <i className="nc-icon nc-ambulance" /> */}
                          <p>Loss of taste and smell</p>
                        
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/sore-throat.svg')}></img>
                          {/* <i className="nc-icon nc-app" /> */}
                          <p>Sore Throat</p>
                        
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/nose.svg')}></img>
                          {/* <i className="nc-icon nc-atom" /> */}
                          <p>Congestion or runny noses</p> 
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/back.svg')}></img>
                          {/* <i className="nc-icon nc-badge" /> */}
                          <p>Muscle or Body Ache</p>
                          
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/vomit.svg')}></img>
                          {/* <i className="nc-icon nc-bag-16" /> */}
                          <p>Vomiting</p>
                      
                        </li>
                       
                      </ul>
                    </section>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
            <Card className="demo-icons">
                <CardHeader>
                  <CardTitle tag="h5">Severe Symtoms</CardTitle>
                  
                </CardHeader>
                <CardBody className="all-icons">
                  <div id="icons-wrapper">
                    <section>
                      <ul>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/breathing.svg')}></img>
                        {/* <i  className="fas fa-thermometer-2 fa-2x" /> */}
                          <p style={{color:"red"}}> Trouble Breathing</p>
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/chest-pain.svg')}></img>
                        {/* <i className="fas fa-thermometer-2 fa-2x" /> */}
                        <p style={{color:"red"}}> Pressure in chest</p>
                        
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/confusion.svg')}></img>
                          {/* <i className="nc-icon nc-alert-circle-i" /> */}
                          <p style={{color:"red"}}>New confusion</p>
                          
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/big-lips.svg')}></img>
                          {/* <i className="nc-icon nc-align-center" /> */}
                          <p style={{color:"red"}}>Bluish Lips or face</p>
                        
                        </li>
                        <li>
                        <img style={{height:"3rem", width: "3rem"}} src={require('assets/img/sleepy.svg')}></img>
                          {/* <i className="nc-icon nc-align-left-2" /> */}
                          <p style={{color:"red"}}>Inability to stay awake</p>
                        
                        </li>
                       
                      </ul>
                    </section>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Icons;
