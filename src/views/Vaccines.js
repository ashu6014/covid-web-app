import React from "react";
import { useEffect, useState } from "react";
import {
  MenuItem,
  FormControl,
  Select
} from "@material-ui/core";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from "reactstrap";

const Tables = () =>{

  const [vaccines, setVaccineInfo] = useState([]);

  const [country, setInputCountry] = useState("worldwide");
  const [countryInfo, setCountryInfo] = useState({});
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    const targetUrl = "https://disease.sh/v3/covid-19/all";
    fetch(targetUrl)
      .then((response) => response.json())
      .then((data) => {
        setCountryInfo(data);
      });
  }, []);  
  
  useEffect(()=>{
      const getCountriesData = async ()=>{
        const targetUrl = "https://disease.sh/v3/covid-19/countries";
        await fetch(targetUrl)
        .then((response)=>response.json())
        .then((data)=>{        
          const countries = data.map((country) =>({
            name: country.country, //name of the country
            value: country.countryInfo.iso2 //iso code of the country
          }) )
          setCountries(countries);
        })
      }
      getCountriesData();      
    }, [])


    const onCountryChange = async (e) => {
      const countryCode = e.target.value;
      const targetUrl =
        countryCode === "worldwide"
          ? "https://disease.sh/v3/covid-19/all"
          : `https://disease.sh/v3/covid-19/countries/${countryCode}`;
      await fetch(targetUrl)
        .then((response) => response.json())
        .then((data) => {
          setInputCountry(countryCode);
          setCountryInfo(data);
        });
    };

  useEffect(() => {
    const getVaccineInfo = async () =>{
      await    fetch("https://disease.sh/v3/covid-19/vaccine")
      .then((response) => response.json())
      .then((vaccineData) => {
        const vaccines = vaccineData.data.map((vaccine)=>({
          name: vaccine.candidate,
          institute: vaccine.institutions,
          trialPhase: vaccine.trialPhase,
          mechanism: vaccine.mechanism
        }))
        setVaccineInfo(vaccines);
      })
    }
    getVaccineInfo();

  }, []);
  

  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h5">Vaccine Information by Country     
                
                <FormControl style={{paddingLeft: "2rem"}} className="country-dropdown" >
  <Select  onChange={onCountryChange} value={country} style={{textTransform:'uppercase', fontWeight:600, fontFamily:"Montserrat, Helvetica Neue, Arial, sans-serif"}}>
    <MenuItem value="worldwide"><b>WORLDWIDE</b></MenuItem>
    {countries.map((country)=>(
         <MenuItem style={{textTransform:'uppercase', fontFamily:"Montserrat, Helvetica Neue, Arial, sans-serif"}}  value={country.value}>{country.name}</MenuItem> 
        ))}  
  </Select>
</FormControl>
           <span tag="h6">
             (*WIP)
           </span>
                </CardTitle>
                
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead className="text-primary">
                    <tr>
                      <th>Vaccine Name</th>
                      <th>Institute</th>
                      <th>Mechanism</th>
                      <th className="text-right">Trial Phase</th>
                    </tr>
                  </thead>
                  <tbody>
                    {vaccines.map((vaccine)=>(
                      <tr>
                      <td>{vaccine.name}</td>
                      <td>{vaccine.institute[0]}</td>
                      <td>{vaccine.mechanism}</td>
                      <td className="text-right">{vaccine.trialPhase}</td>
                    </tr>
                    ))}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}

export default Tables;
