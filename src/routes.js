import Dashboard from "views/Dashboard.js";
import Symptoms from "views/Symptoms.js";
import Vaccines from "views/Vaccines.js";

var routes = [
  {
    path: "/dashboard",
    name: "Global Statistics",
    icon: "fas fa-chart-line",
    component: Dashboard,
    layout: "/admin",
  },
  {
    path: "/symptoms",
    name: "Symptoms",
    icon: "fas fa-clipboard-check",
    component: Symptoms,
    layout: "/admin",
  },
  {
    path: "/vaccine",
    name: "Vaccines",
    icon: "fas fa-syringe",
    component: Vaccines,
    layout: "/admin",
  }
];

export default routes;
